public class Player {
    protected String name;
    protected double position;
    protected Projectile heldProjectile;

    public String getName() {
        return this.name;
    }

    public Double getPosition() {
        return this.position;
    }

    public Player(String name, double position) {
        this.name = name;
        this.position = position;
    }

    public boolean giveProjectile(Projectile p) {
        if (this.heldProjectile == null) {
            this.heldProjectile = p;
            return true;
        } else return false;
    }

    public boolean toss(Target t) {
        if (this.heldProjectile == null) {
            return false;
        } else if (this.heldProjectile != null) {
            this.heldProjectile.beTossed(this, t);
        } return true;
        
    } 

}
