interface Target {
    public void receive(Projectile pri);

    public double getPosition();
}
